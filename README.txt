Introduction
============
This module uses oEmbed to retrieve data from URLs and create song nodes into Drupal.

It also searchs for duplicated content and will add more features for the cancamusic.com website.

Installation
============
This module requires oEmbed module installed. You must enable oEmbed Core and oEmbed Embed.ly modules enabled.

